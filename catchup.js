import { fetch, credentials, pixivToPleroma, known_ids } from "./common.js";
var {pixiv_cookie, pixiv_user_id} = credentials;

var new_ids = [];
top: for (var offset = 0;;) {
	let url = `https://www.pixiv.net/ajax/user/${pixiv_user_id}/illusts/bookmarks?tag=&offset=${offset}&limit=100&rest=show&lang=en&version=5dc84ab282403a049abea4e2f2214b6a69d31da6`;
	let data = await fetch(url, {headers: {Cookie: pixiv_cookie}}).then(res => res.json());
	let ids = data.body.works.map(x => x.id);
	if (!ids.length) break;
	let this_page_has_known_ids = false;
	for (let id of ids) {
		if (id == Deno.args[0]) break top; //init empty db
		if (id in known_ids) {
			this_page_has_known_ids = true;
		} else {
			new_ids.push(id);
		}
	}
	if (this_page_has_known_ids) break;
	offset += ids.length;
}

console.log("new ids", new_ids);

for (let id of new_ids.reverse()) {
	await pixivToPleroma(id);
}