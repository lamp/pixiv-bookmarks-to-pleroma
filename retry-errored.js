import { getAllStatuses, pixivToPleroma } from "./common.js";

var statuses = await getAllStatuses();
statuses = statuses.filter(status => status.content.includes("#error"));
statuses = statuses.map(status => {
	var pixiv_id = status.content.match(/https:\/\/www.pixiv.net\/en\/artworks\/(\d+)/)[1];
	return [status.id, pixiv_id];
});
console.log(statuses.map(([s,p]) => `https://pleroma.lamp.wtf/notice/${s} => https://www.pixiv.net/en/artworks/${p}`).join("\n"));

for (let [status_id, pixiv_id] of statuses) {
	await pixivToPleroma(pixiv_id, status_id);
}