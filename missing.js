import { fetch, credentials, pixivToPleroma, getAllStatuses } from "./common.js";
var {pixiv_cookie, pixiv_user_id} = credentials;

console.log("get all pixiv bookmarks");

var all_bookmark_ids = [];
for (var offset = 0;;) {
	let url = `https://www.pixiv.net/ajax/user/${pixiv_user_id}/illusts/bookmarks?tag=&offset=${offset}&limit=100&rest=show&lang=en&version=5dc84ab282403a049abea4e2f2214b6a69d31da6`;
	let data = await fetch(url, {headers: {Cookie: pixiv_cookie}}).then(res => res.json());
	let works = data.body.works;
	if (!works.length) break;
	offset += works.length;
	let ids = works.filter(w => !w.isMasked).map(x => x.id.toString());
	all_bookmark_ids.push(...ids);
}

console.log(`${all_bookmark_ids.length} ummasked bookmarks`);
console.log("get all pleroma statuses");

var statuses = await getAllStatuses();
console.log(`${statuses.length} statuses`);

statuses = statuses.map(x => x.content);

var missing_ids = [];

for (let id of all_bookmark_ids.reverse()) {
	if (statuses.some(status => status.includes(id) && !status.includes("#error"))) continue;
	missing_ids.push(id);
}

console.log("missing_ids", missing_ids);

confirm();

for (let id of missing_ids) {
	await pixivToPleroma(id);
}