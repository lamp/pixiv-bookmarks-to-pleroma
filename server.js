import { pixivToPleroma } from "./common.js";

Deno.serve({
	port: 18024,
	hostname: "127.0.0.1"
}, async (req, info) => {
	var {pathname} = new URL(req.url);
	console.log(info.remoteAddr.hostname, req.headers.get('x-forwarded-for'), pathname);

	try {
		switch (pathname) {
			case "/ajax/illusts/bookmarks/add":
				var payload = await req.clone().json();
				console.log(payload);
				var {comment, illust_id, restrict, tags} = payload;
				pixivToPleroma(illust_id);
				break;
			case "/ajax/illusts/bookmarks/delete":
				var payload = await req.clone().formData();
				console.log(payload);
				// todo
				break;
			case "/touch/ajax_api/ajax_api.php":
				var payload = await req.clone().formData();
				console.log(payload);
				var mode = payload.get("mode");
				if (mode == "add_bookmark_illust") {
					//var restrict = payload.get("restrict");
					//var tag = payload.get("tag");
					var id = payload.get("id");
					//var comment = payload.get("comment");
					pixivToPleroma(id);
				} else if (mode == "delete_bookmark_illust") {
					var id = payload.get("id");
					// todo
				}
				break;
			//todo mobile app
		}
	} catch (error) {
		console.error("payload error:", error.stack);
		console.debug(req.headers.get("content-type"));
	}

	return fetch("https://www.pixiv.net"+pathname, {
		method: req.method,
		headers: req.headers,
		body: req.body,
		redirect: "manual"
	});
});